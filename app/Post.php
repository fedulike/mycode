<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Tble name change
    //protected $table='posts';
    //change of primary key
   // public $primaryKey='item_id';
    //including timestamps
    //public $timeStamps=true;
    public function user(){
        return $this->belongsTo('App\User');
    }
}
